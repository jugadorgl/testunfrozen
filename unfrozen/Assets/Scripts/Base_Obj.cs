﻿using UnityEngine;


public class Base_Obj : MonoBehaviour
{
	protected	GameObject		Obj;
	protected	Transform		transf;
	protected	Transform		m_parent;


    public virtual void Init()
    {
		Obj			= gameObject;
		transf		= transform;
		m_parent	= transf.parent;
    }

	
	public virtual void Play ()
	{

	}

}
