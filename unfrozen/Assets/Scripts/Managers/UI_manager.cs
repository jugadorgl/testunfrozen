﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_manager : MonoBehaviour
{
    public static UI_manager     insta;
    [SerializeField]private Button[]        m_player_buttons;
    [SerializeField]private Text            m_Round_text;
    public Panel_winner    m_Panel_win;


    void Awake()
    {
        insta   = this;    
    }


    private void Start()
    {
        m_Panel_win.gameObject.SetActive(false);
    }


    public void BlockButtons( bool value )
    {
        for (int i = 0; i < m_player_buttons.Length; i++)
        {
            m_player_buttons[i].interactable = !value;
        }
    }


    public void SetRoundCounter( int _counter )
    {
        m_Round_text.text   = (_counter > 0) ? "Round "+_counter.ToString() : "";
    }
}
