﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Battle_manager : MonoBehaviour
{
    public static Battle_manager    insta;
    [SerializeField]private Back_scroll[]   m_back_scrolls;
    [SerializeField]private List<EUnit>     m_player1_units;
    [SerializeField]private Transform[]     m_player1_points;
    [SerializeField]private List<EUnit>     m_player2_units;
    [SerializeField]private Transform[]     m_player2_points;

    public  Transform       m_Left_point_atack;
    public  Transform       m_Right_point_atack;

    private Team_base       m_player1_Team;
    private Team_base       m_player2_Team;
    private Team_base       m_current_team;

    private int             i_round_counter = 0;


    private void Awake()
    {
        insta = this;
    }


    public void Start()
    {
        SetTargetForBacks( Camera.main.transform );
        m_player1_Team   = new Team_base( m_player1_units, m_player1_points );
        m_player2_Team       = new Team_base( m_player2_units, m_player2_points );

        UI_manager.insta.BlockButtons( true );
        UI_manager.insta.SetRoundCounter( i_round_counter );
        StartCoroutine( IStart() );

    }

    
    public void SetTargetForBacks( Transform _target )
    {
        for (int i = 0; i < m_back_scrolls.Length; i++)
        {
            m_back_scrolls[i].SetTarget( _target );
        }
    }


    IEnumerator IStart()
    {
        yield return new WaitForSeconds(1f);
        NextUnit();
    }

    
    public void NextUnit()
    {
        //Random Team Move
        List<Team_base> _teams = new List<Team_base>();
        if ( m_player1_Team.HaveUnitsForSelection() )    _teams.Add( m_player1_Team );
        if ( m_player2_Team.HaveUnitsForSelection() )    _teams.Add( m_player2_Team );

        if ( _teams.Count == 0 )
        {
            NewRound();
        }
        else
        {
            int rnd = Mathf.RoundToInt( Random.value * (float)(_teams.Count-1) );
            m_current_team  = _teams[ rnd ];

            m_current_team.SelectAtacker();
        }
    }


    private void NewRound()
    {
        i_round_counter++;
        UI_manager.insta.SetRoundCounter( i_round_counter );

        m_player1_Team.UnitsOnRound();
        m_player2_Team.UnitsOnRound();
        NextUnit();
    }


    public void AfterFight()
    {
        if ( !m_player1_Team.HaveUnits() )
        {
            UI_manager.insta.m_Panel_win.Play( false );
        }
        else if ( !m_player2_Team.HaveUnits() )
        {
            UI_manager.insta.m_Panel_win.Play( true );
        }
        else
        {
            NextUnit();
        }
    }


    public void Atack()
    {
       m_current_team.Atack();
    }


    public void Skip()
    {
        m_current_team.Skip();
    }


    public Team_base GetOpponentTeam( Team_base _team )
    {
        return (_team == m_player1_Team) ? m_player2_Team : m_player1_Team;
    }


    public void Quit()
    {
        Application.Quit();
    }
}
