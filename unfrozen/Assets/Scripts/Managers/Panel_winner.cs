﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Panel_winner : MonoBehaviour
{
    [SerializeField]private TextMeshProUGUI m_text;


    public void Play( bool _player1 )
    {
        m_text.text = "Player "+ ( _player1 ? "1" : "2" )+" Win !!!";
        gameObject.SetActive(true);
    }


    public void RestartGame()
    {
        Units_manager.insta.Init();
        Battle_manager.insta.Start();
        gameObject.SetActive(false);
    }
}
