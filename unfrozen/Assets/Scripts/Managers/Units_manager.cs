﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public  delegate void   callBack();

[System.Serializable]
public enum EUnit { none, miner }


[System.Serializable]
public class CUnitParams
{
    public  int         _health;
    public  Vector2Int  _damage;
    public  float       _hitChance  = 0.75f;
}


[System.Serializable]
public class CUnit
{
    [HideInInspector]
    public  string      _name;
    public  EUnit       _unit;
    public  GameObject  _prefab;
    public  List<Unit_base>   _cache;
}


public class Units_manager : MonoBehaviour
{
    public static Units_manager     insta;
    [SerializeField]    CUnit[]     m_units;


    void Awake()
    {
        insta   = this;    
    }


    [ContextMenu("Rename")]
    private void Rename()
    {
        for (int i = 0; i < m_units.Length; i++)
        {
            m_units[i]._name = m_units[i]._unit.ToString();
        }
    }


    public void Init()
    {
        for (int i = 0; i < m_units.Length; i++)
        {
            for (int j = 0; j < m_units[i]._cache.Count; j++)
            {
                if (m_units[i]._cache[j] != null)   Destroy( m_units[i]._cache[j].gameObject );
            }
            m_units[i]._cache.Clear();
        }
    }


    public Unit_base SpawnUnit( EUnit _unit )
    {
        Unit_base new_unit = null;
        for (int i = 0; i < m_units.Length; i++)
        {
            if ( m_units[i]._unit == _unit)
            {
                GameObject go = GameObject.Instantiate( m_units[i]._prefab as Object, transform ) as GameObject;
                new_unit = go.GetComponent<Unit_base>();
                m_units[i]._cache.Add( new_unit );
                break;
            }
        }
        return new_unit;
    }

}
