﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Back_scroll : MonoBehaviour
{
    private Transform   m_target                    = null;
    private Vector3     v_target_init_pos;
    private Vector3     v_default_pos;
    private Vector3	    ref_move_velocity           = Vector3.zero;
    [SerializeField]private Vector2 v_dump_offset   = Vector2.zero;
    [SerializeField]private float   f_speed         = 0.1f;


    void Start()
    {
        v_default_pos   = transform.position;
    }


    void LateUpdate()
    {
        if ( m_target != null)
        {
            Vector3 v_offset    = ( m_target.position - v_target_init_pos ) * v_dump_offset;
            v_offset.z          = v_default_pos.z;
            Vector3 v_target    = v_default_pos + v_offset;
            transform.position  = Vector3.SmoothDamp( transform.position, v_target, ref ref_move_velocity, f_speed);
        }
    }


    public void SetTarget( Transform _target )
    {
        v_target_init_pos   = _target.position;
        m_target = _target;
    }
}
