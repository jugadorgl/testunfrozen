﻿using System.Collections;
using UnityEngine;


public class MovePointToPoint : Base_Obj
{
	public	Transform		m_Point1;
	public	Transform		m_Point2;
	public	Vector2			f_move_time		= new Vector2(0.25f, 0.5f ); //x -delay, y - work time
	
	public	AnimationCurve	a_curve_Scale;

    private  callBack        cbFunc;


	
	public void Play ( bool fromTo, callBack _cb )
	{
		cbFunc	= _cb;

		if (!Obj.activeSelf)
		{
			Obj.SetActive(true);
		}
		if(Obj.activeSelf && Obj.activeInHierarchy)
			StartCoroutine(IMove(fromTo));
	}


	IEnumerator IMove( bool fromTo )
	{
		yield return new WaitForSeconds(f_move_time.x);

		Vector3 pos1 	= fromTo ? m_Point1.position : m_Point2.position;
		Vector3 pos2 	= fromTo ? m_Point2.position : m_Point1.position;

		float	_scale	= 0;
		_scale		= a_curve_Scale.Evaluate( fromTo ? 0 : 1);
		transf.localScale	= new Vector3(_scale, _scale, 1);

		for ( float t = 0.0f; t < 1.0f; t += Time.deltaTime / f_move_time.y )
		{
            Vector3 tPos = Vector3.Lerp(pos1, pos2, t);

			_scale		= a_curve_Scale.Evaluate( fromTo ? t : (1-t) );
			transf.localScale	= new Vector3(_scale, _scale, 1);

			transf.position	= tPos;

			yield return null;
		}

		_scale		= a_curve_Scale.Evaluate( fromTo ? 1 : 0 );
		transf.localScale	= new Vector3(_scale, _scale, 1);

		transf.position	= pos2;

		if (cbFunc != null)
        {
			cbFunc();
			cbFunc	= null;
        }
	}
}
