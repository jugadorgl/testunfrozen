﻿using System.Collections;
using UnityEngine;
using TMPro;


public class Text_blink : Base_Obj
{
    [SerializeField]private TextMeshPro     m_Text;
    private Color       m_defaultColor;
    [SerializeField]private Color           m_BlinkColor;
	public	float			m_TimeBlink		= 0.2f;


    override public void Init () 
	{
		base.Init ();
		m_defaultColor 		= m_Text.color;
	}


    public override void Play ()
	{
		if (!Obj.activeSelf)
		{
			Obj.SetActive(true);
		}

		if (Obj.activeSelf && Obj.activeInHierarchy)
			StartCoroutine( IBlink () );
	}


	IEnumerator IBlink()
	{
		for ( float t = 0.0f; t < 1.0f; t += Time.deltaTime / m_TimeBlink)
		{
			m_Text.color = Color.Lerp(m_defaultColor, m_BlinkColor, t);
			yield return null;
		}
		m_Text.color = m_defaultColor;
	}
}
