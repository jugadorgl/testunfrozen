﻿using System.Collections;
using UnityEngine;


public class TimeScaleRect : Base_Obj
{
	public	RectTransform	transfRect;
	public	AnimationCurve	Curve;

	public	float			m_TimeScale			= 0.2f;
	private	Vector3 		v_default_Scale;


	override public void Init () 
	{
		base.Init ();
		v_default_Scale 		= transfRect.localScale;
	}


	public override void Play ()
	{
		transfRect.localScale	= v_default_Scale;
		if (!Obj.activeSelf)
		{
			Obj.SetActive(true);
		}

		if (Obj.activeSelf && Obj.activeInHierarchy)
			StartCoroutine( IScaling ( Curve) );
	}


    IEnumerator IScaling( AnimationCurve _curve )
	{
		float _Scale				= _curve.Evaluate(0);
		transfRect.localScale		= new Vector3(_Scale, _Scale, 1);
		for ( float t = 0.0f; t < 1.0f; t += Time.deltaTime / m_TimeScale)
		{
			_Scale					= _curve.Evaluate( t );
			transfRect.localScale	= new Vector3(_Scale, _Scale, 1);
			yield return null;
		}
		_Scale						= _curve.Evaluate( 1 );
		transfRect.localScale		= new Vector3(_Scale, _Scale, 1);
	}

}
