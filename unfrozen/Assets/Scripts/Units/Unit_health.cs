﻿using UnityEngine;
using TMPro;


public class Unit_health : MonoBehaviour
{
    [SerializeField]private TextMeshPro     m_text;
    private int                             m_base_health;
    private int                             m_max_health;
    [SerializeField]private Base_Obj[]      m_Obj;

    [ContextMenu("CollectObj")]
    private void CollectObj()
    {
        m_Obj = GetComponentsInChildren<Base_Obj>();
    }


    public void InitHealth( int _hp )
    {
        m_base_health   = _hp;
        m_max_health    = m_base_health;
        for (int i = 0; i < m_Obj.Length; i++)
        {
            m_Obj[i].Init();
        }
        m_text.text     = m_base_health.ToString();
    }


    public void SetDamage ( int _damage )
    {
        if ( _damage > 0 )
        {
            for (int i = 0; i < m_Obj.Length; i++)
            {
                m_Obj[i].Play();
            }

            m_base_health   = Mathf.Clamp( (m_base_health - _damage), 0, m_max_health );
            m_text.text     = m_base_health.ToString();
        }
    }


    public  bool  Live()
    {
        return (m_base_health > 0 ) ? true : false;
    }
}
