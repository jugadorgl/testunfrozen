﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Team_base 
{
    private List<Unit_base> m_units             = new List<Unit_base>();
    private List<Unit_base> m_units_on_round    = new List<Unit_base>();
    private Unit_base       m_choosed;


    public Team_base( List<EUnit> _units, Transform[] _points )
    {
        List<Transform> _p = new List<Transform>();

        for (int i = 0; i < _points.Length; i++)
            _p.Add ( _points[i]);

        m_units.Clear();
        for (int i = 0; i < _units.Count; i++)
        {
            if ( _p.Count > 0 )
            {
                Unit_base _u = Units_manager.insta.SpawnUnit( _units[i] );
                _u.Spawn( _p[0], this );
                m_units.Add ( _u );
                _p.RemoveAt(0);
            }
        }
    }


    public void UnitsOnRound()
    {
        m_units_on_round.Clear();
        for (int i = 0; i < m_units.Count; i++)
        {
            m_units_on_round.Add( m_units[i] );
        }
    }


    public bool HaveUnits()
    {
        return ( m_units.Count > 0 ) ? true : false;
    }


    public bool HaveUnitsForSelection()
    {
        return ( m_units_on_round.Count > 0 ) ? true : false;
    }


    public void SelectAtacker()
    {
        //Random Unit Move
        int rnd = Mathf.RoundToInt( Random.value * (float)(m_units_on_round.Count-1) );
        m_choosed = m_units_on_round[rnd];
        m_choosed.Selected( true, false );
        m_units_on_round.RemoveAt( rnd );

        Battle_manager.insta.SetTargetForBacks( m_choosed.transform );

        UI_manager.insta.BlockButtons( false );
    }


    public Unit_base GetChoosed()
    {
        return m_choosed;
    }


    public void SelectTarget( bool value )
    {
        for (int i = 0; i < m_units.Count; i++)
        {
            m_units[i].Selected( value, false);
            m_units[i].ActivateCollider( value );
        }
    }


    public void Atack()
    {
        Team_base _opponent_team = Battle_manager.insta.GetOpponentTeam( this );
        _opponent_team.SelectTarget( true );
        m_choosed.Selected(true, true);
    }


    public void Skip()
    {
        m_choosed.Selected( false, false );
        Battle_manager.insta.NextUnit();
    }


    public void FightOn( Unit_base _teamUnit )
    {
        UI_manager.insta.BlockButtons(true);
        Team_base _opponent_team = Battle_manager.insta.GetOpponentTeam( this );

        _opponent_team.GetChoosed().Selected( false, false );
        _opponent_team.GetChoosed().ToFight( true, _teamUnit );

        SelectTarget( false );
        _teamUnit.ToFight( false, null );

    }


    public void FightOff( Unit_base _teamUnit )
    {
        if ( _teamUnit.GetHealth().Live())
        {
            _teamUnit.FromFight( null );
        }
        else
        {
            Killed( _teamUnit );
        }
        Team_base _opponent_team = Battle_manager.insta.GetOpponentTeam( this );
        _opponent_team.GetChoosed().FromFight( Battle_manager.insta.AfterFight );
    }


    private void Killed( Unit_base _teamUnit )
    {
        for(int i = 0; i < m_units_on_round.Count; i++ )
        {
            if (m_units_on_round[i] == _teamUnit)
            {
                m_units_on_round.RemoveAt(i);
                break;
            }
        }

        for(int i = 0; i < m_units.Count; i++ )
        {
            if (m_units[i] == _teamUnit)
            {
                m_units.RemoveAt(i);
                break;
            }
        }
    }
}
