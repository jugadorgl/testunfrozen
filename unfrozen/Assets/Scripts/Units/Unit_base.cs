﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;


public class Unit_base : Base_Obj
{
    [SerializeField]private SkeletonAnimation   m_SpineAnimation;
    private  Spine.AnimationState               m_spineAnimationState;
    [SerializeField]private MovePointToPoint    m_Mover;

    private Team_base                           m_owner_team    = null;
    private Unit_base                           m_hittedUnit    = null;

    [SerializeField]private CUnitParams         m_params;
    [SerializeField]private Unit_health         m_health;
    [SerializeField]private Collider2D          m_collider;
    [SerializeField]private SpriteRenderer      m_selector;


    void Start()
    {
        m_Mover.Init();

        m_spineAnimationState = m_SpineAnimation.AnimationState;

        if (m_SpineAnimation != null)
        {
            m_SpineAnimation.AnimationState.Complete += delegate 
            {
                EndAnimation();
            };
            m_SpineAnimation.Skeleton.SetToSetupPose();
        }       
        m_spineAnimationState.ClearTracks();

        IdleAnim();

        m_health.InitHealth( m_params._health );
        
        ActivateCollider( false );

        m_selector.gameObject.SetActive( false );
    }

    
    public virtual void Spawn( Transform _point, Team_base _owner )
    {
        transform.position  = _point.position;
        m_SpineAnimation.transform.localScale  = new Vector3( (_point.position.x < 0) ? 1 : -1, 1, 1);
        m_owner_team        = _owner;
        m_Mover.m_Point1    = _point;
    }


    public void ActivateCollider( bool value )
    {
        m_collider.enabled  = value;
    }


    public void Selected( bool value, bool forAtack )
    {
        if (value)
        {
            if (forAtack)
            {
                m_selector.color = Color.green;
            }
            else
            {
                m_selector.color = Color.yellow;
            }
        }
        m_selector.gameObject.SetActive( value );
    }


    public CUnitParams Getparams()
    {
        return m_params;
    }


    public Unit_health GetHealth()
    {
        return m_health;
    }


    private void OnMouseDown()
    {
        m_owner_team.FightOn( this );
    }


    public void ToFight( bool _atacker, Unit_base _hittedUnit )
    {
        m_Mover.m_Point2 = (transform.position.x < 0 ) ? Battle_manager.insta.m_Left_point_atack : Battle_manager.insta.m_Right_point_atack;
        if (_atacker)
        {
            print("Atack!!!");
            m_hittedUnit    = _hittedUnit;
            m_Mover.Play( true, AtackAnim );
        }
        else
        {
            print("Idle!!!");
            m_Mover.Play( true, IdleAnim );
        }
    }


    public void FromFight( callBack cbFunc)
    {
        m_Mover.Play( false, cbFunc );
    }


    public virtual void IdleAnim()
    {
        m_spineAnimationState.SetAnimation(0, "Idle", true );
    }


    public virtual void AtackAnim()
    {
        print("Atack_anim");
        m_spineAnimationState.SetAnimation(0, "Miner_1", false );
    }


    public virtual void DamagedAnim( int _damage )
    {
        print("Damage_anim");
        m_spineAnimationState.SetAnimation( 0, "Damage", false );
        m_health.SetDamage( _damage );
    }

    
    public virtual void MissHitAnim()
    {
        print("Miss_anim");
        m_spineAnimationState.SetAnimation( 0, "Pull", false );
    }


    public virtual  void EndAnimation()
    {
        string _animation = m_SpineAnimation.AnimationState.GetCurrent(0).ToString();

        if ( _animation == "Miner_1")
        {
            if ( Random.value <= m_hittedUnit.Getparams()._hitChance )
            {
                int _damage = (int)( Random.Range( m_params._damage.x, m_params._damage.y) );
                m_hittedUnit.DamagedAnim( _damage );
            }
            else
            {
                m_hittedUnit.MissHitAnim();
            }
            IdleAnim();
        }
        else if ( ( _animation == "Damage" ) || ( _animation == "Pull" ) )
        {
            m_owner_team.FightOff( this );
            if ( !m_health.Live() )
            {
                Destroy ( gameObject );
            }
            else
            {
                IdleAnim();
            }
        }
    }

}
